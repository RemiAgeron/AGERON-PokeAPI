// Import of GOT Package to Collect Raw Data
import got from 'got'

// Custom type for Raw Data from API
type Pokemon = {
  name: string,
  url: string,
  types: [string]
}

// Extract from API the Type of each Pokemon
async function typeData(pokemon: Pokemon): Promise<[string]> {
  return got.get(pokemon.url).json()
    .then((data: any): [string] => data.types.map((e: any): string => e.type.name))
}

// Format the Data
async function mapData(limit: number): Promise<any> {
  
  // API Url
  let urlLimit: string = `https://pokeapi.co/api/v2/pokemon?limit=${limit}`

  // Get Raw Data from API
  let rawData: [Pokemon] = await got.get(urlLimit).json()
    .then((data: any): [Pokemon] => data.results)

  // MAP the Data like the target and returned it
  let map = new Map()

  for await (const pokemon of rawData) {
    const types = await typeData(pokemon)
    types.forEach(type => {
      if (map.has(type)) {
        map.set(type, map.get(type).concat(pokemon.name))
      } else {
        map.set(type, [pokemon.name])
      }
    })
  }

  return map
}

// Display the Data
console.log(await mapData(50))