var __asyncValues = (this && this.__asyncValues) || function (o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
};
// Import of GOT Package to Collect Raw Data
import got from 'got';
// Extract from API the Type of each Pokemon
async function typeData(pokemon) {
    return got.get(pokemon.url).json()
        .then((data) => data.types.map((e) => e.type.name));
}
async function mapData(limit) {
    var e_1, _a;
    // API Url
    let urlLimit = `https://pokeapi.co/api/v2/pokemon?limit=${limit}`;
    // Get Raw Data from API
    let rawData = await got.get(urlLimit).json()
        .then((data) => data.results);
    // MAP the Data like the target
    let map = new Map();
    try {
        for (var rawData_1 = __asyncValues(rawData), rawData_1_1; rawData_1_1 = await rawData_1.next(), !rawData_1_1.done;) {
            const pokemon = rawData_1_1.value;
            const types = await typeData(pokemon);
            types.forEach(type => {
                if (map.has(type)) {
                    map.set(type, map.get(type).concat(pokemon.name));
                }
                else {
                    map.set(type, [pokemon.name]);
                }
            });
        }
    }
    catch (e_1_1) { e_1 = { error: e_1_1 }; }
    finally {
        try {
            if (rawData_1_1 && !rawData_1_1.done && (_a = rawData_1.return)) await _a.call(rawData_1);
        }
        finally { if (e_1) throw e_1.error; }
    }
    return map;
}
console.log(await mapData(50));
